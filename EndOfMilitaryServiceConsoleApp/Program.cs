﻿using System;

namespace EndOfMilitaryServiceConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter start day: ");

            var day = short.Parse(Console.ReadLine() ?? string.Empty);

            Console.WriteLine("enter start month: ");

            var month = short.Parse(Console.ReadLine() ?? string.Empty);

            Console.WriteLine("enter start year: ");

            var year = short.Parse(Console.ReadLine() ?? string.Empty);

            Military.StartDateTime = new DateTime(year, month, day);

            var hours = Military.StartDateTime.Hour;
            var minutes = Military.StartDateTime.Minute;
            var seconds = Military.StartDateTime.Second;

            Console.WriteLine("enter discount day: ");

            Military.DiscountDayNumber = short.Parse(Console.ReadLine() ?? string.Empty);

            Console.WriteLine("enter extra day: ");

            Military.ExtraDayNumber = short.Parse(Console.ReadLine() ?? string.Empty);

            var timeSpan = new TimeSpan(638, hours, minutes, seconds);

            Military.EndDateTime = Military.StartDateTime + timeSpan;

            Military.FreedomDateTime = Military.EndDateTime.AddDays(Military.ExtraDayNumber)
                .Subtract(TimeSpan.FromDays(Military.DiscountDayNumber));

            Console.WriteLine("freedom date is : " + Military.FreedomDateTime.ToString("yyyy-MM-dd"));

            Console.ReadKey();
        }
    }
}

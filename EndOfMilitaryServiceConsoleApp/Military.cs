﻿using System;

namespace EndOfMilitaryServiceConsoleApp
{
    public static class Military
    {
        public static DateTime StartDateTime { get; set; }
        public static DateTime EndDateTime { get; set; }
        public static int DiscountDayNumber { get; set; }
        public static int ExtraDayNumber { get; set; }
        public static DateTime FreedomDateTime { get; set; }
    }
}
